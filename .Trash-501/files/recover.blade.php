Hello {{ $email }}, <br><br>

Klikk denne linken for å aktivere nytt passord. <br><br>

Nytt passord: {{ $password }}<br><br>

--- <br>
<a href="{{ $link }}">{{ $link }}</a> <br>
--- <br>