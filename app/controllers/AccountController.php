<?php

/**
* @version 19.02.2014
* @author Martin Othamar <martin@othamar.net>
*/



class AccountController extends BaseController {



	public function postSignIn() {
		$validator = Validator::make(Input::all(), 
		array(
			'epost' => 'required|email',
			'passord' => 'required'
		));

		if($validator->fails()) {
			return Redirect::route('home')
					->withErrors($validator)
					->withInput();
		} else {
			$remember = (Input::has('remember')) ? true : false;

			$auth = Auth::attempt(array(
				'epost' => Input::get('epost'),
				'password' => Input::get('passord'),
				'active' => 1
			), $remember);

			if($auth) {
				return Redirect::route('home')
						->with('global', 'Du er logget inn');
			} else {
				return Redirect::route('home')
						->with('red', 'Informasjonen er ikke riktig.');
			}
		}

		return Redirect::route('home')
				->with('global', 'Du kunne ikke logges inn, har du aktivert?');
 	}


 	public function getSignOut() {
 		Auth::logout();
 		return Redirect::route('home');
 	}


	public function postCreate() {
		$validator = Validator::make(Input::all(), 
			array(
				'epost' => 'required|max:50|email|unique:users',
				'passord' => 'required|min:6|max:60',
				'repeter_passord' => 'required|max:60|same:passord'
		));

		if($validator->fails()) {
			return Redirect::route('home')
					->withErrors($validator)
					->withInput();
		} else {
			$epost = Input::get('epost');
			$password = Input::get('passord');
			$code = str_random(60);

			$user = User::create(array(
				'epost' => $epost,
				'password' => Hash::make($password),
				'code' => $code,
				'active' => 0
			));

			if($user) {
				Mail::send('epost.auth.activate', array('link' => URL::route('bruker-aktiver', $code), 
					'epost' => $epost), 
					function($message) use ($user) {
						$message->to($user->epost, '$user->epost')->subject('Aktiver din konto!');
				});

				return Redirect::route('home')
						->with('global', 'Din konto har blitt registrert. Du vil få en epost med aktiveringsinfo.');
			}
		}
	}


	public function getActivate($code) {
		$user = User::where('code', '=', $code)->where('active', '=', 0);

		if($user->count()) {
			$user = $user->first();
			$user->active = 1;
			$user->code = '';

			if($user->save()) {
				return Redirect::route('home')
						->with('global', 'Din konto har blitt aktivert, du kan nå logge inn.');
			}
		}

		return Redirect::route('home')
				->with('global', 'Vi kunne ikke aktivere kontoen din.');
	}


	public function getChangePassword() {
		return View::make('bruker.endre-passord');
	}


	public function postChangePassword() {
		$validator = Validator::make(Input::all(), array(
			'gammelt_passord' => 'required',
			'passord' => 'required|min:6',
			'repeter_passord' => 'required|same:passord'
		));

		if($validator->fails()) {
			return Redirect::route('bruker-endre-passord')
					->withErrors($validator);
		} else {
			$user = User::find(Auth::user()->id);

			$gammelt_passord = Input::get('gammelt_passord');
			$passord = Input::get('passord');

			if(Hash::check($gammelt_passord, $user->getAuthPassword())) {
				$user->password = Hash::make($passord);

				if($user->save()) {
					return Redirect::route('home')
							->with('global', 'Passordet har blitt endret');
				}
			} else {
				return Redirect::route('bruker-endre-passord')
				->with('global', 'Ditt gamle passord stemmer ikke.');
			}
		}

		return Redirect::route('bruker-endre-passord')
				->with('global', 'Passordet kunne ikke endres.');
	}


	public function getForgotPassword() {
		return View::make('bruker.glemt-passord');
	}


	public function postForgotPassword() {
		$validator = Validator::make(Input::all(), array(
			'epost' => 'required|email'
		));

		if($validator->fails())  {
			return Redirect::route('home')
					->withErrors($validator)
					->withInput();
		} else {
			$user = User::where('epost', "=", Input::get('epost'));

			if($user->count()) {
				$user = $user->first();
				$code = str_random(60);
				$password = str_random(10);

				$user->code = $code;
				$user->password_temp = Hash::make($password);

				if($user->save()) {
					Mail::send('epost.auth.forgot',
						array('link' => URL::route('bruker-gjenopprett', $code), 'epost' => $user->epost, 'password' => $password),
						function ($message) use ($user) {
							$message->to($user->epost, $user->id)->subject('Brukergjenoppretting');
						});
					return Redirect::route('home')
							->with('global', 'Vi har nå sendt deg mail med veiledning til gjennoppretting av kontoen din.');
				}
			} 
		}

		return Redirect::route('home')
				->with('global', 'Kunne ikke generere.');
	}


	public function getRecover($code) {
		$user = User::where('code', '=', $code)
				->where('password_temp', '!=', '');
		if($user->count()) {
			$user = $user->first();
			$user->password = $user->password_temp;
			$user->password_temp = '';
			$user->code = '';

			if($user->save()) {
				return Redirect::route('home')
						->with('global', 'Kontoen din er gjennomrettet og du kan logge inn med det midlertidige passordet oppgitt i mailen. 
							Du kan endre dette passordet via "Endre passord" i brukermenyen når du er logget inn.');
			}
		}

		return Redirect::route('home')
				->with('global', 'Kunne ikke tilbakestille kontoinformasjon.');
	}

}