<?php

/**
* @version 19.02.2014
* @author Martin Othamar <martin@othamar.net>
*/



class HomeController extends BaseController {
	


	public function home() {

			
			$params = array(
				'redirect_uri' => url('/'),
				'scope' => 'email'
			);

            return View::make('home');
        }
        
}