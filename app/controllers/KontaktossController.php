<?php

/**
* @version 20.02.2014
* @author Martin Othamar <martin@othamar.net>
*/



class KontaktossController extends BaseController {


	public function kontaktoss() {
		return View::make('kontakt-oss');
	}


	public function postMail() {
		Mail::send();
	}

}