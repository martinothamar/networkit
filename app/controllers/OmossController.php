<?php

/**
* @version 20.02.2014
* @author Martin Othamar <martin@othamar.net>
*/



class OmossController extends BaseController {


	public function omoss() {
		return View::make('om-oss');
	}

}