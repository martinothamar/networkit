<?php

/**
* @version 19.02.2014
* @author Martin Othamar <martin@othamar.net>
*/



class ProfileController extends BaseController {


	public function user($id) {
		$user = User::where('id', '=', $id);

		if($user->count()) {
			$user = $user->first();
			$data = array(
				'user' => $user, 
				'edu' => unserialize($user->education0),
				'loc' => unserialize($user->location)
			);

			return View::make('profil.bruker')
			 		->with('data', $data);
		}

		return App::abort(404);
	}

}