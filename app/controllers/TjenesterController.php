<?php

/**
* @version 20.02.2014
* @author Martin Othamar <martin@othamar.net>
*/



class TjenesterController extends BaseController {
	

	public function tjenester() {
		return View::make('tjenester');
	}

}