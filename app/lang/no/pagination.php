<?php 

/**
 * Norwegian language file: pagination.php
 *
 * @package  Language
 * @version  3.2.13
 * @author   Joachim Martinsen <joachim@martinsen.is>
 * @link     http://www.martinsen.is/
 */

return array(

	/*
	|--------------------------------------------------------------------------
	| Pagination Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are used by the paginator library to build
	| the pagination links. You're free to change them to anything you want.
	| If you come up with something more exciting, let us know.
	|
	*/

	'previous' => '&laquo; Forrige',
	'next'     => 'Neste &raquo;',

);