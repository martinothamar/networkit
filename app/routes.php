<?php

/**
* @version 19.02.2014
* @author Martin Othamar <martin@othamar.net>
*/


/**
* Get index
*/
Route::get('/', array(
    'as' => 'home',
    'uses' => 'HomeController@home'
));


/**
* Get Om oss route
*/
Route::get('/om-oss', array(
	'as' => 'om-oss',
	'uses' => 'OmossController@omoss'
));


/**
* Get Våre tjenester route
*/
Route::get('/tjenester', array(
	'as' => 'tjenester',
	'uses' => 'TjenesterController@tjenester'
));


/**
* Get Kontakt oss route
*/
Route::get('/kontakt-oss', array(
	'as' => 'kontakt-oss',
	'uses' => 'KontaktossController@kontaktoss'
));


/**
* Profile (GET)
*/
Route::get('/profil/{id}', array(
	'as' => 'profil-bruker',
	'uses' => 'ProfileController@user'
));



/**
* Auth group
*/
Route::group(array('before' => 'auth'), function() {


	/**
	* Auth CSRF group
	*/
	Route::group(array('before' => 'csrf'), function() {
		/**
		* Change password (POST)
		*/
		Route::post('/bruker/endre-passord', array(
			'as' => 'bruker-endre-passord-post',
			'uses' => 'AccountController@postChangePassword'
		));
	});


	/**
	* Change password (GET)
	*/
	Route::get('/bruker/endre-passord', array(
		'as' => 'bruker-endre-passord',
		'uses' => 'AccountController@getChangePassword'
	));


	/**
	* Sign out (GET)
	*/
	Route::get('/bruker/logg-ut', array(
		'as' => 'bruker-logg-ut',
		'uses' => 'AccountController@getSignOut'
	));
});



/**
* Unauth group
*/
Route::group(array('before' => 'guest'), function() {
	/**
	* CSRF protection group
	*/
	Route::group(array('before' => 'csrf'), function() {
		/**
		* Create account (post)
		*/
		Route::post('/bruker/registrer', array(
			'as' => 'bruker-registrer-post',
			'uses' => 'AccountController@postCreate'
		));


		/**
		* Sign in (post)
		*/
		Route::post('/bruker/logg-inn', array(
			'as' => 'bruker-logg-inn-post',
			'uses' => 'AccountController@postSignIn'
		));


		/**
		* Forgot password (POST)
		*/
		Route::post('/bruker/glemt-passord', array(
			'as' => 'bruker-glemt-passord-post',
			'uses' => 'AccountController@postForgotPassword'
		));
	});


	/**
	* Recover account-password (GET)
	*/
	Route::get('/bruker/gjenopprett/{code}', array(
		'as' => 'bruker-gjenopprett',
		'uses' => 'AccountController@getRecover'
	));


	/**
	* Get activation link
	*/
	Route::get('/bruker/aktiver/{code}', array(
		'as' => 'bruker-aktiver',
		'uses' => 'AccountController@getActivate'
	));


	Route::get('/logg-inn/fb', function() {
	    $facebook = new Facebook(Config::get('facebook'));
	    $params = array(
	        'redirect_uri' => 'http://rugoth.asuscomm.com/logg-inn/fb/code',
	        'scope' => 'email,user_birthday,user_about_me,user_education_history,user_location,friends_about_me,friends_location,',
	    );
	    return Redirect::to($facebook->getLoginUrl($params));
	});


	Route::get('/logg-inn/fb/code', array(
		'as' => 'logg-inn-fb-code',
		'uses' => 'FacebookController@signIn'
	));

});