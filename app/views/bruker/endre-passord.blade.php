@extends('layout.main')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-lg-6 well">
			<form action="{{ URL::route('bruker-endre-passord-post') }}" method="post">

				<div class="input-group">
					<input type="password" name="gammelt_passord" class="form-control pwd3" placeholder="Gammelt passord">
					<span class="input-group-btn">
			            <button class="btn btn-default reveal3" type="button"><i class="glyphicon glyphicon-eye-open"></i></button>
			        </span>
					@if($errors->has('gammelt_passord'))
						{{ $errors->first('gammelt_passord') }}
					@endif
				</div><br>

				<div class="input-group">
					<input type="password" name="passord" class="form-control pwd4" placeholder="Nytt passord">
					<span class="input-group-btn">
			            <button class="btn btn-default reveal4" type="button"><i class="glyphicon glyphicon-eye-open"></i></button>
			        </span>
					@if($errors->has('passord'))
						{{ $errors->first('passord') }}
					@endif
				</div>

				<div class="input-group">
					<input type="password" name="repeter_passord" class="form-control pwd5" placeholder="Repeter nytt passord">
					<span class="input-group-btn">
			            <button class="btn btn-default reveal5" type="button"><i class="glyphicon glyphicon-eye-open"></i></button>
			        </span>
					@if($errors->has('repeter_passord'))
						{{ $errors->first('repeter_passord') }}
					@endif
				</div><br>

				<button type="submit" class="btn btn-default" mame="endre_passord">Endre passord
				{{ Form::token() }}
			</form>
		</div>
	</div>
</div>
@stop