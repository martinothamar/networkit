<form action="{{ URL::route('bruker-glemt-passord-post') }}" method="post">
	<p>Oppgi eposten du registrerte med i boksen under, så sender vi deg et midlertidig passord.</p>
	<div class="form-group">
		<input type="text" name="epost" class="form-control" placeholder="Din epost" {{ (Input::old('epost')) ? ' value="' .e(Input::old('epost')). '"' : '' }}>
		@if($errors->has('epost'))
			{{ $errors->first('epost') }}
		@endif 
	</div>

	<button type="submit" class="btn btn-default" mame="send">Send midlertidig passord

	{{ Form::token() }}
</form>