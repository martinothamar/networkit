<div class="modal fade bs-modal-sm" id="logg-inn" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
		    <br>
		    <div class="bs-example bs-example-tabs">
		        <ul id="myTab" class="nav nav-tabs">
		          	<li class="active"><a href="#logbruker" data-toggle="tab">Bruker</a></li>
		          	<li class=""><a href="#logbedrift" data-toggle="tab">Bedrift</a></li>
		          	<li class=""><a href="#glemtpassord" data-toggle="tab">Glemt passord</a></li>
		        </ul>
		    </div>
		<div class="modal-body">
		    <div id="myTabContent" class="tab-content">
		    	<div class="tab-pane fade active in" id="logbruker">
		        
					<form action="{{ URL::route('bruker-logg-inn-post') }}" method="post">
						<p>Logg inn</p>
						<hr>
						<p>Logg inn via en av dine eksisterende kontoer...</p><br>
						<a class="btn btn-block btn-social btn-facebook" href="http://rugoth.asuscomm.com/logg-inn/fb">
					    	<i class="fa fa-facebook"></i> Logg inn med facebook
						</a>

						<hr>	
						<div class="input-group">
							<span class="input-group-addon">@</span>
							<input type="text" name="epost" class="form-control" placeholder="Epost-adresse" {{ (Input::old('epost')) ? ' value="'. Input::old('epost') .'"' : '' }}>
							@if($errors->has('epost'))
								{{ $errors->first('epost') }}
							@endif
						</div>

						<div class="input-group">
							<span class="input-group-addon glyphicon glyphicon-lock"></span>
							<input type="password" name="passord" class="form-control pwd2" placeholder="Passord">
							<span class="input-group-btn">
				            	<button class="btn btn-default reveal2" type="button"><i class="glyphicon glyphicon-eye-open"></i></button>
				        	</span>
							@if($errors->has('passord'))
								{{ $errors->first('passord') }}
							@endif
						</div>

						<div class="checkbox">
					        <label>
					          	<input type="checkbox" name="remember" value="remember"> Husk meg
					        </label>
				      	</div>

						<button type="submit" class="btn btn-default" mame="logg_inn">Logg inn
						{{ Form::token() }}
					</form>

		    	</div>
		    	<div class="tab-pane fade" id="logbedrift">
		    		<p>Her kommer omsider innlogging for bedriftskontoer.</p>
		  		</div>
		  		<div class="tab-pane fade" id="glemtpassord">
		       		@include('bruker.glemt-passord')
		  		</div>
			</div>
		</div>
			<div class="modal-footer">
				<center>
					<button type="button" class="btn btn-default" data-dismiss="modal">Lukk</button>
				</center>
			</div>
		</div>
	</div>
</div>