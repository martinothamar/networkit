<div class="modal fade bs-modal-md" id="registrer" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
		    <br>
		    <div class="bs-example bs-example-tabs">
		        <ul id="myTab" class="nav nav-tabs">
		          	<li class="active"><a href="#regbruker" data-toggle="tab">Bruker</a></li>
		          	<li class=""><a href="#regbedrift" data-toggle="tab">Bedrift</a></li>
		          	<li class=""><a href="#hvorfor" data-toggle="tab">Hvorfor?</a></li>
		        </ul>
		    </div>
		<div class="modal-body">
		    <div id="myTabContent" class="tab-content">
		    	<div class="tab-pane fade active in" id="regbruker">

		        	<form action="{{ URL::route('bruker-registrer-post') }}" method="post">
						<p>Brukerinformasjon</p>
						<hr>
						<div class="input-group">
							<span class="input-group-addon">@</span>
							<input type="text" name="epost" class="form-control" placeholder="Din epost-adresse" {{ (Input::old('epost')) ? ' value="'. e(Input::old('epost')) .'"' : '' }}>
							
						</div>

						<div class="input-group">
							<span class="input-group-addon glyphicon glyphicon-lock"></span>
							<input type="password" name="passord" class="form-control pwd0" placeholder="Passord">
							<span class="input-group-btn">
					            <button class="btn btn-default reveal0" type="button"><i class="glyphicon glyphicon-eye-open"></i></button>
					        </span>
							
						</div>

						<div class="input-group">
							<span class="input-group-addon glyphicon glyphicon-lock"></span>
							<input type="password" name="repeter_passord" class="form-control pwd1" placeholder="Repeter ditt passord">
							<span class="input-group-btn">
					            <button class="btn btn-default reveal1" type="button"><i class="glyphicon glyphicon-eye-open"></i></button>
					        </span>
							
						</div>
						<hr>
						<button type="submit" class="btn btn-default" mame="registrer">Registrer bruker
						{{ Form::token() }}
					</form>

		    	</div>
		    	<div class="tab-pane fade" id="regbedrift">
		    		<p>Test</p>
		  		</div>
		  		<div class="tab-pane fade" id="hvorfor">
		  			<p>Fordi!</p>
		  		</div>
			</div>
		</div>
			<div class="modal-footer">
				<center>
					<button type="button" class="btn btn-default" data-dismiss="modal">Lukk</button>
				</center>
			</div>
		</div>
	</div>
</div>