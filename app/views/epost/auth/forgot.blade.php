Hei {{ $epost }}, <br><br>

Klikk følgende link for å gjenopprette kontoen din: <br>

--- <br>
<a href="{{ $link }}">{{ $link }}</a> <br>
--- <br><br>

Får du melding om godkjent gjenoppretting, kan du logge inn med dette midlertidige passordet: {{ $password }}<br><br>
Du kan deretter endre passordet via 'Endre passord' i brukermenyen.<br>

Dersom du ikke har bedt om gjenoppretting kan du ignorere denne mailen og varsle oss på martin@othamar.net<br><br>

Mvh NetworkIT