@extends('layout.main')

@section('content')
	<div id="myCarousel" class="carousel slide color1">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner">
            <div class="item active">
                <div class="fill" style=""></div> <!-- background-image:url('http://placehold.it/1900x1080&amp;text=Slide One'); -->
                <div class="carousel-caption">
                    <h1>Velkommen til NetworkIT</h1>
                </div>
            </div>
            <div class="item">
                <div class="fill" style=""></div>
                <div class="carousel-caption">
                    <h1></h1>
                </div>
            </div>
            <div class="item">
                <div class="fill" style=""></div>
                <div class="carousel-caption">
                    <h1></a>
                    </h1>
                </div>
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>
    </div>
@stop