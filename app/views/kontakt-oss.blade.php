@extends('layout.main')

@section('content')
<!-- Page Content -->

<div class="container">
      
  <div class="row">
  
    <div class="col-lg-12">
      <h1 class="page-header">Kontakt oss <small>Vi ser frem til å høre fra deg!</small></h1>
      <ol class="breadcrumb">
        <li><a href="{{ URL::route('home') }}">Hjem</a></li>
        <li class="active">Kontakt oss</li>
      </ol>
    </div>
    
    <div class="col-lg-12">
      <!-- Embedded Google Map using an iframe - to select your location find it on Google maps and paste the link as the iframe src. If you want to use the Google Maps API instead then have at it! -->
      <iframe width="100%" height="400" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?q=maps+oslo&amp;ie=UTF8&amp;hq=&amp;hnear=Oslo,+Norge&amp;t=m&amp;z=10&amp;ll=59.913869,10.752245&amp;output=embed"></iframe><br /><small><a href="https://maps.google.com/maps?q=maps+oslo&amp;ie=UTF8&amp;hq=&amp;hnear=Oslo,+Norge&amp;t=m&amp;z=10&amp;ll=59.913869,10.752245&amp;source=embed" style="color:#0000FF;text-align:left">Vis større kart</a></small>
    </div>

  </div><!-- /.row -->
  
  <div class="row">

    <div class="col-sm-8">
      <h3>Ta gjerne kontakt med oss!</h3>
      <p>Dersom du har spørsmål til våre ansatte er det bare å ta kontakt. Vi lover å svare deg i løpet av 72 timer. Vi snakkes!</p>
		<form role="form" method="POST" action="contact-form-submission.php">
            <div class="row">
              <div class="form-group col-lg-4">
                <label for="input1">Navn</label>
                <input type="text" name="contact_name" class="form-control" id="input1">
              </div>
              <div class="form-group col-lg-4">
                <label for="input2">Epost-adresse</label>
                <input type="email" name="contact_email" class="form-control" id="input2">
              </div>
              <div class="form-group col-lg-4">
                <label for="input3">Mobilnummer</label>
                <input type="phone" name="contact_phone" class="form-control" id="input3">
              </div>
              <div class="clearfix"></div>
              <div class="form-group col-lg-12">
                <label for="input4">Melding</label>
                <textarea name="contact_message" class="form-control" rows="6" id="input4"></textarea>
              </div>
              <div class="form-group col-lg-12">
                <input type="hidden" name="save" value="contact">
                <button type="submit" class="btn btn-default">Send</button>
              </div>
          </div>
        </form>
    </div>

    <div class="col-sm-4">
      <h3>NetworkIT</h3>
      <h4>En rekrutteringsportal</h4>
      <p>
        Gunhilds vei 4 F<br>
        4625, Kristiansand<br>
      </p>
       <p><i class="fa fa-phone"></i> <abbr title="Telefonnummer">T</abbr>: +47 47 35 60 90</p>
      <p><i class="fa fa-envelope-o"></i> <abbr title="Epost-adresse">E</abbr>: <a href="mailto:martin@othamar.net">martin@othamar.net</a></p>
      <p><i class="fa fa-clock-o"></i> <abbr title="Åpningstider">Å</abbr>: For deg, 24/7.</p>
      <ul class="list-unstyled list-inline list-social-icons">
        <li class="tooltip-social facebook-link"><a href="#facebook-page" data-toggle="tooltip" data-placement="top" title="Facebook"><i class="fa fa-facebook-square fa-2x"></i></a></li>
        <li class="tooltip-social linkedin-link"><a href="#linkedin-company-page" data-toggle="tooltip" data-placement="top" title="LinkedIn"><i class="fa fa-linkedin-square fa-2x"></i></a></li>
        <li class="tooltip-social twitter-link"><a href="#twitter-profile" data-toggle="tooltip" data-placement="top" title="Twitter"><i class="fa fa-twitter-square fa-2x"></i></a></li>
        <li class="tooltip-social google-plus-link"><a href="#google-plus-page" data-toggle="tooltip" data-placement="top" title="Google+"><i class="fa fa-google-plus-square fa-2x"></i></a></li>
      </ul>
    </div>

  </div><!-- /.row -->

</div><!-- /.container -->
@stop