<div class="container">

    <hr>

    <footer>
        <div class="row">
            <div class="col-lg-12">
                <p>Copyright &copy; NetworkIT @ 2014</p>
            </div>
        </div>
    </footer>

</div>
<!-- /.container -->


<!-- JavaScript -->
<script src="/js/jquery-1.10.2.js"></script>
<script src="/js/bootstrap.js"></script>
<script src="/js/modern-business.js"></script>
<script src="/js/revealpw.js"></script>
<script src="/js/modal.js"></script>
@include('messages.global')
@include('messages.red')
@include('messages.green')