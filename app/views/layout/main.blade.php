<!DOCTYPE html>
<html lang="nb">
	<head>
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <meta name="description" content="">
	    <meta name="author" content="Martin Othamar">

	    <title>NetworkIT</title>
	    <link rel="shortcut icon" href="/img/networkit.ico">

	    <!-- CSS -->
	    <link href="/css/main.css" rel="stylesheet">
	    <link href="/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	    <link href="/css/stickyfooter.css" rel="stylesheet">
	    <link href="/css/custom.css" rel="stylesheet">
	    <link href="/css/bootstrap-social.css" rel="stylesheet">

	</head>
	<body>
		@include('layout.navigation')
		@if(!Auth::check())
			@include('bruker.logg-inn')
		@endif
		@include('bruker.registrer')
		@yield('content')
		@include('layout.footer')
	</body>
</html>