<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!-- You'll want to use a responsive image option so this logo looks good on devices - I recommend using something like retina.js (do a quick Google search for it and you'll find it) -->
            <a class="navbar-brand" href="{{ URL::route('home') }}">NetworkIT</a>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="{{ URL::route('om-oss') }}">Om oss</a>
                </li>
                <li><a href="{{ URL::route('tjenester') }}">Våre tjenester</a>
                </li>
                <li><a href="{{ URL::route('kontakt-oss') }}">Kontakt oss</a>
                </li>
                @if(Auth::check())
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ Auth::user()->epost }} <b class="caret"></b></a>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ URL::route('profil-bruker', Auth::user()->id) }}">Min profil</a></li>
                        <li><a href="{{ URL::route('bruker-endre-passord') }}">Endre passord</a></li>
                        <li class="divider"></li>
                        <li><a href="{{ URL::route('bruker-logg-ut') }}">Logg ut</a></li>
                    </ul>
                </li>
                @else
                </li><li><a href="#logg-inn" data-toggle="modal" data-target="#logg-inn">Logg inn</a></li>
                @endif
                </li><li><a href="#registrer" data-toggle="modal" data-target="#registrer">Registrer</a></li>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav><br>