@if(Session::has('green'))
<div class="modal fade bs-modal-md" id="green" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    ×</button>
                <span class="glyphicon glyphicon-hand-right"></span> <strong>Det skjedde noe feil...</strong>
                <hr class="message-inner-separator">
           
                    <div class="alert alert-success">{{ Session::get('green') }}</div>

                <div class="modal-footer">
                    <center>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Lukk</button>
                    </center>
                </div>
            </div>
        </div>
    </div>
</div>
<script>$('#green').modal();</script>
@endif