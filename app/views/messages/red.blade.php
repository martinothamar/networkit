@if(Session::has('red') || $errors->has())
<div class="modal fade bs-modal-md" id="red" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    ×</button>
                <span class="glyphicon glyphicon-hand-right"></span> <strong>Det skjedde noe feil...</strong>
                <hr class="message-inner-separator">
                <div class="alert alert-danger">
                    {{ Session::get('red') }}
                    @foreach ($errors->all() as $error)
                        {{ $error }}
                    @endforeach
                </div>
                <div class="modal-footer">
                    <center>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Lukk</button>
                    </center>
                </div>
            </div>
        </div>
    </div>
</div>
<script>$('#red').modal();</script>
@endif