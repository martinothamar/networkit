@extends('layout.main')

@section('content')<br><br>
<div class="container">

    <div class="row">

        <div class="col-lg-12">
            <h1 class="page-header">{{ $data['user']->fname, ' ', $data['user']->lname }}
                <small>"Status-tekst"</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ URL::route('home') }}">Hjem</a>
                </li>
                <li class="active">Profil</li>
            </ol>
        </div>

    </div>
    <!-- /.row -->

    <div class="row">

        <div class="col-lg-12">
            <img class="img-responsive" src="{{ $data['user']->photo }}">
            <a href="{{ $data['user']->links }}">Facebook</a>
        </div>

    </div>
    <!-- /.row -->


    <div class="row">

        <div class="col-md-8">
            <h2 class="page-header">Utdanning</h2>
            <li>{{ $data['edu'][0][1]['concentration'][0]['name'], ' ved ', $data['edu'][0][1]['school']['name'] }}</li>
            <li>{{ $data['edu'][0][0]['classes'][0]['name'], ' ved ', $data['edu'][0][0]['school']['name'] }}</li>
        </div>

    </div>
</div>

@stop