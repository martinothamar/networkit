@extends('layout.main')

@section('content')
<!-- Page Content -->

<div class="container">

    <div class="row">

        <div class="col-lg-12">
            <h1 class="page-header">Tjenester
                <small>Hva vi kan gjøre for deg</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ URL::route('home') }}">Hjem</a>
                </li>
                <li class="active">Tjenester</li>
            </ol>
        </div>

    </div>
    <!-- /.row -->

    <div class="row">

        <div class="col-lg-12">
            <img class="img-responsive" src="/img/top_banner_business_consultants.jpg">
        </div>

    </div>
    <!-- /.row -->

    <!-- Service Paragraphs -->

    <div class="row">

        <div class="col-md-8">
            <h2 class="page-header">Premium-tjeneste for bedrifter</h2>
            <p>Er du en daglig leder eller personal/rekrutterings-ansvarlig i en bedrift eller organisasjon? Vi kan hjelpe deg å finne drømmekandidater til ditt embete på rekord tid! Deltakere i vår Premium-tjeneste får tilgang og eksklusive rettigheter til våre heteste kandidater og får blant annet ukentlige nyhetsbrev med drømmestudenter som er spesialtilpasset dine behov. Finn ut mer <a href="">her</a>.</p>
        </div>

        <div class="col-md-4">
            <h2 class="page-header">Interessert?</h2>
            <p>Vår tjenester vil være klar umiddelbart, takket være våres høyst automatiserte databasesystem.</p>
            <a class="btn btn-default" href="#">Bli med!</a>
        </div>

    </div>
    <!-- /.row -->

    <!-- Service Tabs -->

    <div class="row">

        <div class="col-lg-12">
            <h2 class="page-header">Andre tjenester</h2>
            <ul id="myTab" class="nav nav-tabs">
                <li class="active"><a href="#service-one" data-toggle="tab">Gull-profil</a>
                </li>
                <li><a href="#service-two" data-toggle="tab">Konsultasjon</a>
                </li>
                <li><a href="#service-three" data-toggle="tab">Annonser</a>
                </li>

            </ul>
            <div id="myTabContent" class="tab-content">
                <div class="tab-pane fade in active" id="service-one">
                    <i class="fa fa-gear pull-left fa-4x"></i>
                    <p>Nyt ubegrenset tilgang til alt av funksjonalitet og tiltrekk deg de største og beste bedriftene ved å oppnå større mulighet for tilpasning av din profil og image utad. <a href="">Klikk her for å benytt deg av tilbudet!</a> <b>BONUS: 1 gratis konsultasjon dersom du melder deg inn før 01.08.2014!</b></p>
                </div>
                <div class="tab-pane fade" id="service-two">
                    <i class="fa fa-gears pull-left fa-4x"></i>
                    <p>Rådfør deg med våre karrierespesialister og optimaliser din sjanse for å bli plukket opp til drømmejobben. Du får velge fritt blant våre konsulenter og får den første timen gratis. <a href="">Bli med!</a></p>
                </div>
                <div class="tab-pane fade" id="service-three">
                    <i class="fa fa-magic pull-left fa-4x"></i>
                    <p>Vi har en rekke muligheter for ekstra annonser på nettsiden. Ta kontakt med konsulent for nærmere info.</p>
                </div>
            </div>
        </div>

    </div>
    <!-- /.row -->

    <!-- Service Images -->

    <div class="row">

        <div class="col-lg-12">
            <h2 class="page-header">Vår service</h2>
        </div>

        <div class="col-sm-4">
            <img class="img-responsive" src="http://placehold.it/750x450">
            <h3>Gull-profilen</h3>
            <p>Her får du en liten smakebit på hvilke goder du får på kjøpet av en Gull-profil.</p>
            <a class="btn btn-link btn-sm pull-right">Se mer <i class="fa fa-angle-right"></i></a>
        </div>

        <div class="col-sm-4">
            <img class="img-responsive" src="http://placehold.it/750x450">
            <h3>Konsultasjon</h3>
            <p>Våre konsulenter har god utdanning og ikke minst erfaring i rekruttering og karrierebygging. Her er en av våre beste konsulenter.</p>
            <a class="btn btn-link btn-sm pull-right">Se mer <i class="fa fa-angle-right"></i></a>
        </div>

        <div class="col-sm-4">
            <img class="img-responsive" src="http://placehold.it/750x450">
            <h3>Annonser</h3>
            <p>Se hvordan det kan se ut dersom du bestemmer deg for å leie annonser på NetworkIT.</p>
            <a class="btn btn-link btn-sm pull-right">Se mer <i class="fa fa-angle-right"></i></a>
        </div>

    </div>
    <!-- /.row -->

</div>
<!-- /.container -->
@stop